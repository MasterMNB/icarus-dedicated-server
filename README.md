# Icarus dedicated game server 
This dedicated server will automatically download/update to the latest available server version when started. The dedicated server runs in Ubuntu 22.04 and wine.  
Can also used for pterodactyl (still in DEV STATE!)  
Environment variables for all settings and launch parameters.  

# Moved to Github [Link](https://github.com/MasterMNB/icarus-dedicated-server)
[Docker Hub](https://hub.docker.com/r/mastermnb/icarus-dedicated-server)  
[Based on Repo](https://gitlab.com/fred-beauch/icarus-dedicated-server)  

## License
MIT License